<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>S4: OOP ACTIVITY</title>
</head>
<body>
	<h1>Building</h1>
	<p>The name of the building is <?= $building->getName(); ?></p>
	<p>The <?= $building->getName(); ?> has <?= $building->getFloors(); ?> floors.</p>
	<p>The <?= $building->getName(); ?> is located at <?= $building->getAddress(); ?></p>


	<?php $building->setName('Caswynn Complex'); ?>
	<p>The name of the building has been change to <?= $building->getName(); ?></p>

	<h1>Condominium</h1>
	<p>The name of the building is <?= $condominium->getName(); ?></p>
	<p>The <?= $building->getName(); ?> has <?= $condominium->getFloors(); ?> floors.</p>
	<p>The <?= $building->getName(); ?> is located at <?= $condominium->getAddress(); ?></p>
	
	<?php $condominium->setName('Enzo Tower'); ?>
	<p>The name of the condominium has been change to <?= $condominium->getName(); ?></p>

</body>
</html>