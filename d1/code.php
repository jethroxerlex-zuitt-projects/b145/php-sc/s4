<?php 

class Building {
	/*
		Public vs Private vs Protected:

		When a property or method's access modifier is set to public, those properties/methods can always be directly accessed or modified

		When set to private, they can NOT be directly accessed or modified, AND child classes will not properly inherit private properties at all

		Protected keeps properties from being directly accessed or modified, but also ensures that child classes copy the access modifier properly.

	*/


	protected $name;
	protected $floors;
	protected $address;


	public function __construct($name,$floors,$address){
		$this->name = $name;
		$this->floors = $floors;
		$this->address = $address;
	}
};

class Condominium extends Building {
	/*
		Getters and Setters
			Getter and setter functions serve as intermediaries in accessing or modifying an object's property.


	*/


	public function getName(){
		return $this->name;
	}
}

class Apartment extends Building{
	public function getName(){
		return $this->name;
	}
	//setter
	public function setName($name){
		$this->name = $name;
	}
}


$building = new Building("Caswynn Building",8,"Quezon City,Philippines");
$condominium = new Condominium("Enzo Condo",5,"Makati City,Philippines");
